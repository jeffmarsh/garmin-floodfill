using Toybox.WatchUi as WatchUi;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;
using Toybox.Math as Mat;

class SimpleWatchView extends WatchUi.WatchFace {

	var customFont = null;
	var filledFont = null;
	

    function onLayout(dc) {
        setLayout(Rez.Layouts.WatchFace(dc));
        customFont = WatchUi.loadResource(Rez.Fonts.customFont);
        filledFont = WatchUi.loadResource(Rez.Fonts.filledFont);
    }


    function onShow() {
    }
    
	function drawTime(dc){
		var timeColor = Application.getApp().getProperty("FFTimeColor");
		var clockTime = Sys.getClockTime();
		
		var hour = clockTime.hour;
		var min = clockTime.min;
		        
		if (!Sys.getDeviceSettings().is24Hour){
			hour = hour % 12;
			if (hour == 0){
				hour=12;
			}	
		}
		
		var hourFrmt = hour.format("%02d");
		var minFrmt = min.format("%02d");
		dc.setColor(timeColor, Gfx.COLOR_TRANSPARENT);
		  
		    
		dc.drawText(90,-10,customFont,hourFrmt.toString(),Gfx.TEXT_JUSTIFY_LEFT);
		dc.drawText(95,90,filledFont,minFrmt.toString(),Gfx.TEXT_JUSTIFY_LEFT);
	}
	
	function drawGraphics(dc, boxY){
		// draw line between hour and minute
		var lineColor = Application.getApp().getProperty("FFLineColor");
		var lineY = boxY + 9;
		dc.setColor(lineColor, Gfx.COLOR_TRANSPARENT);
		dc.fillRectangle(0, boxY, dc.getWidth(), 5);
		dc.drawLine(0, lineY, dc.getWidth(), lineY);
	}
	
	function drawSteps(dc, boxY){
		var floodColor = Application.getApp().getProperty("FFStepsColor");
		var textColor = Application.getApp().getProperty("FFStepsTextColor");
		var stepCount = ActivityMonitor.getInfo().steps;
		var stepGoal = ActivityMonitor.getInfo().stepGoal;
		var stepPercent = Math.floor((stepCount * 1.0 / stepGoal * 1.0) * 100).toNumber();
		if (stepPercent >= 100){
			stepPercent = 100;
		}
		dc.setColor(floodColor, Gfx.COLOR_TRANSPARENT);
		dc.fillRectangle(11, 0, 73, stepPercent+15);
		dc.setColor(textColor, Gfx.COLOR_TRANSPARENT);
		dc.drawText(50, boxY-30, Gfx.FONT_SMALL, stepCount.toString(), Gfx.TEXT_JUSTIFY_CENTER);
	}
	
	function drawBattery(dc, boxY){
		var floodColor = Application.getApp().getProperty("FFBatteryColor");
		var textColor = Application.getApp().getProperty("FFBatteryTextColor");
		var batteryPercent = Sys.getSystemStats().battery;
		var batFrmt = batteryPercent.format("%02d");
		
		// calculate battery percentage graph
		var batHeight = 225-batteryPercent;

		//battery percentage
		var batColor = floodColor;
		if (batteryPercent < 25){
			batColor = 0xFF0000;
		}
		dc.setColor(batColor, Gfx.COLOR_TRANSPARENT);
		dc.fillRectangle(11, batHeight, 73, 240-batHeight);
		dc.setColor(textColor, Gfx.COLOR_TRANSPARENT);
		dc.drawText(50, boxY+10, Gfx.FONT_SMALL,batFrmt.toString(), Gfx.TEXT_JUSTIFY_CENTER);
	}
	
	function drawBackground(dc){
    	var color = Application.getApp().getProperty("FFBackgroundColor");
    	dc.setColor(color, Gfx.COLOR_TRANSPARENT);
    	dc.fillRectangle(0, 0, dc.getWidth(), dc.getHeight());
    }

	function onUpdate(dc) {
		dc.clear();
		var boxY = (dc.getHeight()/2)-5;
		drawBackground(dc);
		drawTime(dc);
		drawGraphics(dc, boxY);
		drawSteps(dc, boxY);
		drawBattery(dc, boxY);
	}

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

    // The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() {
    }

    // Terminate any active timers and prepare for slow updates.
    function onEnterSleep() {
    }

}
